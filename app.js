//Instanciation de Vue 
const app = Vue.createApp({
    //data est une propriété qui est une fonction
    data() {
        //L'objet de retour va contenir toutes nos variables réactives
        return {
            //On crée nos variables ici !!
            numberCSS: 2,
            title: 'Vue <strong>Tracker</strong>',
            logoSource: "https://cdn.svgporn.com/logos/vue.svg",
            tsFormater: Intl.DateTimeFormat('fr', { hour: '2-digit', minute: '2-digit' }),
            tasks: [],
            taskId: 0,
            taskname: '',
            isTaskInProgress: false,
            startTime: null,
            nowTime: null,
            intervalEverySecond: null,
            errorMessage: null
        }
    },
    /* Computed properties = variables réactives à d'autre variables, on crée une propriété qu'on peut appeler dans le DOM simplement */
    /* PAr défaut ce sont juste des getters on peut pas modfier la propriété mais on peut en faire un setter ! */
    /* Différence: les méthodes sont rééxécutés dès qu'on met le rendu à jour, pas les computed */
    computed: {
        currentDuration() {
            if (this.startTime && this.nowTime) {
                return this.durationBetweenTimeStamps(this.startTime, this.nowTime)
            }

            return '00:00:00'
        },
        btnClasses() {
            return { 'btn-danger': this.isTaskInProgress, 'btn-primary': !this.isTaskInProgress }
        }
    },
    /* watcher vont être appelé lorsqu'une variable change (fonction du nom de la variable) */
    watch: {
        isTaskInProgress(isInProgress) {
            if (isInProgress) {
                //Attention !!! fonction fléché sinon ça marche pas parce qu'on aurait redéfinit this
                this.intervalEverySecond = setInterval(() => {
                    this.nowTime = Date.now()
                }, 1000);
            } else {
                clearInterval(this.intervalEverySecond)
            }
        }
    },
    methods: {
        startTask() {
            //verification
            if (this.taskname.length === 0) {
                this.errorMessage = "Le nom de la tâche ne peut pas être vide"
                return
            } else if (this.isTaskInProgress) {
                this.errorMessage = "Une tâche est déjà en cours"
            } else {
                this.errorMessage = null
            }

            //Debut de la tâche
            this.isTaskInProgress = true;
            this.startTime = Date.now()
            this.nowTime - Date.now()

        },
        stopTask() {

            //Vérification
            if (!this.isTaskInProgress) {
                this.errorMessage = "Aucune tâche est en cours"
                return
            }


            //Enregistrement de la tâche
            this.tasks.unshift({
                id: this.getAnId(),
                name: this.taskname,
                time: this.startTime,
                end: Date.now()
            })

            //Fin de la tâche
            this.isTaskInProgress = false;
            this.errorMessage = null
            this.nowTime = null
            this.taskname = ""

        },
        toggleTask() {
            if (this.isTaskInProgress) {
                this.stopTask()
            } else {
                this.startTask()
            }
        },
        deleteTask(taskId) {
            let taskIndex = null

            //On récupère l'index de la tâche dans le tableau grâce à son ID 
            this.tasks.forEach((task, index) => {
                if (task.id === taskId) {
                    taskIndex = index
                }
            })

            //On supprime un élèment depuis l'index taskIndex
            this.tasks.splice(taskIndex, 1)
        },
        getAnId() {
            this.taskId++
            return this.taskId
        },
        formatTimeStamp(ts) {
            return this.tsFormater.format(ts)
        },
        durationBetweenTimeStamps(start, end) {
            let seconds = Math.floor((end / 1000) - (start / 1000))
            let minutes = Math.floor(seconds / 60)
            let hours = Math.floor(minutes / 60)
            seconds = seconds % 60
            minutes = minutes % 60

            return `${String(hours).padStart(2, '0')}:${String(minutes).padStart(2, '0')}:${String(seconds).padStart(2, '0')}`
        }
    },
    created() {
        console.log('Appli créé');
    },
    mounted() {
        console.log('Appli mounted');
    }
})

/* On peut faire de l'interpolation {{}} dans les composants comme dans le html */
/* Ici on va lors du click exécuter sendDelete qui va emit 'delete' qui va être reçu dans l'appel du component et qui va exécuté ce qu'on lui précise. On a donc envoyé un élément du component enfant vers le root component */
app.component('task-actions', {
    template: `
        <button @click="sendDelete" type="button" class="btn-danger" style="line§height: 1">
            <svg height="15" xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16" />
            </svg>
        </button>
    `,
    //props définit ce qu'on passe comme params au component dsans le html
    props: {
        id: {
            type: Number,
            required: true
            //Posibilité de mettre default si pas de required à true 
        }
    },
    methods: {
        sendDelete() {
            //On envoie un évènement qui s'appelle delete por le capturer dans le component html avec v-on:delete
            this.$emit('delete', this.id)
        }
    },
    //data du component 
    data() {
        return {
            fakeID: 2
        }
    }
})

app.mount('#app')

/* //Données réactives car quand on change de valeur comme ici, tous les endroits où c'est utilisé vont être actualisés !
setTimeout(() => {
    app.$data.title = 'VueTracker'
}, 1000);

//On peut accéder aux data réactives de app comme ceci : 
console.log(app.$data.title); */